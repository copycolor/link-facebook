import { Injectable } from '@angular/core';
import { 
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument 
} from 'angularfire2/firestore';

import { Product } from '../models/Product';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductService {
  ProductsCollection: AngularFirestoreCollection<Product>;
  Products: Observable<Product[]>;
  ProductDoc: AngularFirestoreDocument<Product>;

  constructor(public afs:AngularFirestore) {
    this.ProductsCollection = this.afs.collection('Products');
    // this.Products = this.afs.collection('Products').valueChanges();
    this.Products = this.ProductsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Product;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getProducts() {
    return this.Products; 
  }

  addProduct(Product: Product) {
    this.ProductsCollection.add(Product);
  }

  deleteProduct(Product: Product) {
    this.ProductDoc = this.afs.doc(`Products/${Product.id}`);
    this.ProductDoc.delete();
  }

  updateProduct(Product: Product) {
    this.ProductDoc = this.afs.doc(`Products/${Product.id}`);
    this.ProductDoc.update(Product);
  }
}
