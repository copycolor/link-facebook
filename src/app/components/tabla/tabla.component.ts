import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ProductService } from '../../services/Product.service';

import { Product } from '../../models/Product';
@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent implements OnInit {
  Products: Product[];
  editState: boolean = false;
  ProductToEdit: Product;
  encapsulation: ViewEncapsulation.None
  display: boolean = false;
  display2: boolean = false;
  
  constructor(public ProductService: ProductService) { }

  ngOnInit() {
    this.ProductService.getProducts().subscribe(Products => {
      //console.log(Products);
      this.Products = Products;
    });
  }

  showDialog() {
    this.display = true;
}

showDialog2() {
  this.display2 = true;
}
}
